<?php
include '../header.php';
include '../navbar.php';

$typeform = $_GET["typeform"];

if ($typeform == "alumno") { ?>

<!-- Formulario Alumno -->
<div class="jumbotron">
    <h1>Registrar Alumno</h1>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="div-form">
                <form action="../registrar/dbreg.php?role=es" method="POST">
                    <div class="form-group">
                        <label for="rolAlumno">ROL USM</label>
                        <input type="text" class="form-control" name="rolAlumno" id="rolAlumno" required>
                    </div>    

                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre">
                    </div>

                    <div class="form-group">
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" name="apellido" id="apellido">
                    </div>

                    <div class="form-group">
                        <label for="anoIngreso">Año de Ingreso</label>
                        <input type="number" class="form-control" name="anoIngreso" id="anoIngreso">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Insertar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Formulario Ayudante -->
<?php } elseif ($typeform == "ayudante"){ ?>

<div class="jumbotron">
    <h1>Registrar Ayudante</h1>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="div-form">
                <form action="../registrar/dbreg.php?role=ay" method="POST">
                    <div class="form-group">
                        <label for="rolAyudante">ROL USM</label>
                        <input type="text" class="form-control" name="rolAyudante" id="rolAyudante" required>
                    </div>    

                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre">
                    </div>

                    <div class="form-group">
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" name="apellido" id="apellido">
                    </div>

                    <div class="form-group">
                        <label for="cantidadSemestres">Cantidad de Semestres</label>
                        <input type="number" class="form-control" name="cantidadSemestres" id="cantidadSemestres">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Insertar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Formulario Profesor -->
<?php } elseif ($typeform == "profesor"){ ?>

<div class="jumbotron">
    <h1>Registrar Profesor</h1>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="div-form">
                <form action="../registrar/dbreg.php?role=pf" method="POST">
                    <div class="form-group">
                        <label for="idProfesor">Id del Profesor</label>
                        <input type="number" class="form-control" name="idProfesor" id="idProfesor" required>
                    </div>    

                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre">
                    </div>

                    <div class="form-group">
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" name="apellido" id="apellido">
                    </div>

                    <div class="form-group">
                        <label for="especialidad">Especialidad</label>
                        <input type="text" class="form-control" name="especialidad" id="especialidad">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Insertar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php } else {
    echo "<div class=\"jumbotron\"><h1>Error!</h1><p>Haz llegado por error a esta página, 
    continúa accediendo a alguno de los links que se encuentran en la barra de navegación</div> ";
} 

include '../footer.php';
?>