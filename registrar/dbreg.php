<?php
include "../header.php";
include "../navbar.php";

if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $role = $_GET["role"];
    $success = FALSE;
    $exists = FALSE;
    
    if ($role == "es"){

        $rolAlumno = $_POST["rolAlumno"];
        $nombre = $_POST["nombre"];
        $apellido = $_POST["apellido"];
        $anoIngreso = $_POST["anoIngreso"];

        $status_query = 'SELECT * FROM alumno WHERE rolalumno = $1';
        $status_result = pg_query_params($dbconn, $status_query, array($rolAlumno));
        $value = pg_fetch_row($status_result);
        
        $exists = ($value[0] != FALSE) ? TRUE : FALSE;
        
        //la comprobacion de existencia solo es al registrar los alumnos como se indica en la tarea
        
        if(!$exists){
            $pgsql_query = 'INSERT INTO alumno (rolalumno, nombre, apellido, anoingreso) VALUES($1,$2,$3,$4)';
            $result = pg_query_params($dbconn, $pgsql_query, array($rolAlumno,$nombre,$apellido,$anoIngreso));
        }        
    }
    elseif ($role == "ay"){
        $rolAyudante = $_POST["rolAyudante"];
        $nombre = $_POST["nombre"];
        $apellido = $_POST["apellido"];
        $cantidadSemestres = $_POST["cantidadSemestres"];
        
        $pgsql_query = 'INSERT INTO ayudante (rolayudante, nombre, apellido, cantidadsemestres) VALUES($1,$2,$3,$4)';
        $result = pg_query_params($dbconn, $pgsql_query, array($rolAyudante,$nombre,$apellido,$cantidadSemestres));
    
    }
    elseif ($role == "pf"){
        $idProfesor = $_POST["idProfesor"];
        $nombre = $_POST["nombre"];
        $apellido = $_POST["apellido"];
        $especialidad = $_POST["especialidad"];
        
        $pgsql_query = 'INSERT INTO profesor (idprofesor, nombre, apellido, especialidad) VALUES($1,$2,$3,$4)';
        $result = pg_query_params($dbconn, $pgsql_query, array($idProfesor,$nombre,$apellido,$especialidad));
        
    }
    else{
        echo "<div class=\"jumbotron\"><h1>Error!</h1><p>Haz llegado por error a esta página, 
        continúa accediendo a alguno de los links que se encuentran en la barra de navegación</div> ";
        $result = FALSE;
    }

    $success = ($result != FALSE) ? TRUE : FALSE;
    include 'regUpdate.php';
}

?>