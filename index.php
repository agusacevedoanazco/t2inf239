<?php
include 'header.php';
include 'navbar.php';
?>

<div class="jumbotron">
    <div class="container">
        <h1>Bienvenido</h1>
        <p>Bienvenido a la página de administración de "The United Mercenaries Association", en ésta pagina puede Ingresar
        distintas misiones para motivar a los ayudantes a asistir a los estudiantes que están complicados con sus ramos.
        Por lo tanto, los profesores pueden añadir misiones sobre un alumno, las cuales los ayudantes pueden aceptar y
        llevarse una recompensa asociada si completan la misión.</p>
        <p>Para comenzar puedes ingresa a las opciones que se encuentran en la barra de navegación superior o alguno de los
        botones que se encuentran en la parte inferior de esta sección.</p>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h2>Registrar</h2>
            <p>Agrega alumnos, ayudantes o profesores según desees.</p>
            <p><a class="btn btn-secondary" href="../registrar/formRegistrar.php?typeform=alumno">Registrar Alumno >></a></p>
            <p><a class="btn btn-secondary" href="../registrar/formRegistrar.php?typeform=ayudante">Registrar Ayudante >></a></p>
            <p><a class="btn btn-secondary" href="../registrar/formRegistrar.php?typeform=profesor">Registrar Profesor >></a></p>
        </div>
        <div class="col-md-4">
            <h2>Administrar Misiones</h2>
            <p>Cambia configuraciones de las misiones existentes, puedes cambiar el estado de la misión, si ésta fue completada o no,
                cambiar la recompensa de la misión o asignar alguna misión a un ayudante que se encuentre disponible.</p>
            <p><a class="btn btn-secondary" href="../misiones/formMision.php?typeform=toggle">Actualizar Estado >></a></p>
            <p><a class="btn btn-secondary" href="../misiones/formMision.php?typeform=reward">Cambiar Recompensa >></a></p>
            <p><a class="btn btn-secondary" href="../misiones/formMision.php?typeform=assign">Asignar Mision >></a></p>
        </div>
        <div class="col-md-4">
            <h2>Ingresar Mision</h2>
            <p>Ingresa una nueva misión para ayudar a los complicados estudiantes</p>
            <p><a class="btn btn-success" href="../misiones/ingMision.php">Ingresar Mision >></a></p>
        </div>
    </div>
</div>

<?php
include 'footer.php';
?>