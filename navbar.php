<body>
<nav class="navbar navbar-expand-sm bg-primary navbar-dark justify-content-end">

    <a class="navbar-brand" href="../index.php">
        <!-- this icon is free to use -->
        <img src="https://img.icons8.com/color/48/000000/home.png" alt="home" style="width:32px;"/>
    </a>
    
    

    <ul class="navbar-nav">
        <!--  Registrar -->
        <li class="nav-item dropdown active">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Registrar</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="../registrar/formRegistrar.php?typeform=alumno">Alumno</a>
                <a class="dropdown-item" href="../registrar/formRegistrar.php?typeform=ayudante">Ayudante</a>
                <a class="dropdown-item" href="../registrar/formRegistrar.php?typeform=profesor">Profesor</a>
            </div>
        </li>

        <li class="nav-item dropdown active">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Administrar Misiones</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="../misiones/formMision.php?typeform=toggle">Actualizar Estado</a>
                <a class="dropdown-item" href="../misiones/formMision.php?typeform=reward">Cambiar Recompensa</a>
                <a class="dropdown-item" href="../misiones/formMision.php?typeform=assign">Asignar Misión</a>
            </div>
        </li>

    </ul>

    <a class="btn btn-success ml-auto mr-1" href="../misiones/ingMision.php">Ingresar Misión</a>
</nav>