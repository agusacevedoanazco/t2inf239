<?php
if($type == "new"){
    if ($success == true){
        echo "<div class=\"jumbotron\"><h1>Nueva misión agregada satisfactoriamente.</h1><p>Id de mision: ".$idm[0]."</p></div>";
    }
    else{
        echo "<div class=\"jumbotron\"><h1>Error! Hubo un error al ingresar la misión.</h1></div>";
    }  
}
elseif($type == "toggle"){
        if(!$exists){
            echo "<div class=\"jumbotron\"><h1>Error! El id de misión no es valido.</h1></div>";
        }
        else{
            if($success){
                echo "<div class=\"jumbotron\"><h1>Se ha actualizado el estado de la misión</h1>";
                echo "<h4>El nuevo estado de la misión es: ";
                if($new_state == 1) echo "Completada</h4></div>";
                else echo "Incompleta</h4></div>";
            }
            else{
                echo "<div class=\"jumbotron\"><h1>Error! Hubo un error al actualizar el estado de la misión.</h1></div>";
            }
        }
    }
elseif($type == "reward"){
    if(!$exists){
        echo "<div class=\"jumbotron\"><h1>Error! El id de misión no es valido.</h1></div>";
    }
    else{
        if($success){
            echo "<div class=\"jumbotron\"><h1>Se ha actualizado la recompensa de la misión</h1></div>";
        }
        else{
            echo "<div class=\"jumbotron\"><h1>Error! Hubo un error al actualizar el estado de la misión.</h1></div>";
        }
    }
}
elseif($type == "assign"){
        if($success){
            echo "<div class=\"jumbotron\"><h1>Misión asignada con exito.</h1></div>";
        }
        else{
            echo "<div class=\"jumbotron\"><h1>Error! Hubo un error al asignar la misión al ayudante.</h1></div>";
        }
    }
else{
    echo "<div class=\"jumbotron\"><h1>Error! Error al ingresar los datos.</h1></div>";
}

?>

<?php include "../footer.php";?>
