<?php
include '../header.php';
include '../navbar.php';

$typeform = $_GET["typeform"];

if ($typeform == "toggle") { ?>

<!-- Formulario Actualizar estado Mision -->
<div class="jumbotron">
    <h1>Actualizar Estado de la Mision</h1>
    <p>Modifica el estado de la misión, al presionar "Actualizar", cambiarás el estado de la misión de incompleta a completa o viceversa según el estado actual de ésta.</p>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="div-form">
                <form action="../misiones/dbmis.php?type=toggle" method="POST">
                    <div class="form-group">
                        <label for="idmision">Id de Mision</label>
                        <input type="number" class="form-control" name="idmision" id="idmision" required>
                    </div>
                    
                    <button type="submit" class="btn btn-success">Actualizar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Actualizar Recompensa -->
<?php } elseif ($typeform == "reward"){ ?>

<div class="jumbotron">
    <h1>Cambiar Recompensa</h1>
    <p>Cambia, actualiza o ingresa una nueva recompensa para la misión con el numero identificador ingresado</p>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="div-form">
                <form action="../misiones/dbmis.php?type=reward" method="POST">
                    <div class="form-group">
                        <label for="idmision">Identificador de la Mision</label>
                        <input type="number" class="form-control" name="idmision" id="idmision" required>
                    </div> 
                    <div class="form-group">
                        <label for="recompensa">Recompensa</label>
                        <input type="text" class="form-control" name="recompensa" id="recompensa" required>
                    </div>
                    
                    <button type="submit" class="btn btn-success">Ingresar Recompensa</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Asignar Mision Ayudante -->
<?php } elseif ($typeform == "assign"){ ?>

<div class="jumbotron">
    <h1>Asignar Mision</h1>
    <p>Asignar una misión existente a un ayudante que haya aceptado la misión</p>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="div-form">
                <form action="../misiones/dbmis.php?type=assign" method="POST">
                    <div class="form-group">
                        <label for="idmision">Id de misión</label>
                        <input type="number" class="form-control" name="idmision" id="idmision" required>
                    </div>    

                    <div class="form-group">
                        <label>ROL Ayudante</label>
                        <input type="text" class="form-control" name="idayudante" id="idayudante" required>
                    </div>
                    
                    <button type="submit" class="btn btn-success">Insertar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php } else {
    echo "<div class=\"jumbotron\"><h1>Error!</h1><p>Haz llegado por error a esta página, 
    continúa accediendo a alguno de los links que se encuentran en la barra de navegación</div> ";
} 

include '../footer.php';
?>