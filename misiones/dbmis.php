<?php
include "../header.php";
include "../navbar.php";

if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $type = $_GET["type"];
    $success = FALSE;
    $idm = null;
    
    if ($type == "new"){
        $idprofesor = $_POST["idprofesor"];
        $rolAlumno = $_POST["rolAlumno"];
        $descripcion = $_POST["descripcion"];
        $recompensa = $_POST["recompensa"];
                
        $pgsql_query = 'INSERT INTO mision (idprofesor, idalumno, descripcion, recompensa) VALUES($1,$2,$3,$4)';
        $result = pg_query_params($dbconn, $pgsql_query, array($idprofesor,$rolAlumno,$descripcion,$recompensa));
        $success = ($result != FALSE) ? TRUE : FALSE;
        $idm = pg_fetch_row(pg_query($dbconn,'SELECT MAX (idmision) FROM mision'));
    }

    elseif ($type == "toggle"){
        $exists = FALSE;
        $idmision = $_POST["idmision"];
        $exist_query = 'SELECT * FROM mision WHERE idmision = $1';
        $result = pg_query_params($dbconn, $exist_query, array($idmision));
        $data = pg_fetch_row($result);
        if($data != null){
            $new_state = ($data[6] == 0) ? 1 : 0;
            $exists = true;
            $upd_query = 'UPDATE mision SET estado = $1 WHERE idmision = $2';
            $result = pg_query_params($dbconn, $upd_query, array($new_state, $idmision));
            $success = ($result != FALSE) ? TRUE : FALSE;
        }
        else{
            $exists = false;
        }
    }

    elseif ($type == "reward"){
        $exists = FALSE;
        $idmision = $_POST["idmision"];
        $exist_query = 'SELECT * FROM mision WHERE idmision = $1';
        $result = pg_query_params($dbconn, $exist_query, array($idmision));
        $data = pg_fetch_row($result);
        if($data != null){
            $new_reward = $_POST["recompensa"];
            $exists = true;
            $result = pg_query_params($dbconn, 'UPDATE mision SET recompensa = $1 WHERE idmision = $2', array($new_reward,$idmision));
            $success = ($result != FALSE) ? true:false;
        }
        else{
            $exists = false;
        }
    }

    elseif ($type == "assign"){
        $idmision = $_POST["idmision"];
        $idayudante = $_POST["idayudante"];

        $pgsql_query = 'INSERT INTO asignacion (idmision, rolayudante) VALUES($1,$2)';
        $result = pg_query_params($dbconn, $pgsql_query, array($idmision,$idayudante));
        $success = ($result != false) ? true:false;
    }
    
    include "../misiones/misionUpdate.php";
}

?>