<?php
include '../header.php';
include '../navbar.php';
//ingresar profesor, rolalumno, descripcion, recompensa
?>

<div class="jumbotron">
    <h1>Añadir Misión</h1>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="div-form">
                <form action="dbmis.php?type=new" method="POST">
                    <div class="form-group">
                        <label for="idprofesor">Id Profesor</label>
                        <input type="number" class="form-control" name="idprofesor" id="idprofesor" required>
                    </div>    

                    <div class="form-group">
                        <label for="rolAlumno">Rol Alumno</label>
                        <input type="text" class="form-control" name="rolAlumno" id="rolAlumno" required>
                    </div>

                    <div class="form-group">
                        <label for="Descripcion">Descripcion</label>
                        <input type="text" class="form-control" name="descripcion" id="descripcion">
                    </div>

                    <div class="form-group">
                        <label for="recompensa">Recompensa</label>
                        <input type="text" class="form-control" name="recompensa" id="recompensa">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Insertar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include '../footer.php'; ?>