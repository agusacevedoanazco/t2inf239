CREATE TABLE Alumno(
	rolAlumno VARCHAR(15) PRIMARY KEY,
	nombre VARCHAR(45),
	apellido VARCHAR(45),
	anoingreso INTEGER
);

CREATE TABLE Profesor(
	idProfesor INTEGER PRIMARY KEY,
	nombre VARCHAR(45),
	apellido VARCHAR(45),
	especialidad VARCHAR(45)
);

CREATE TABLE Ayudante(
	rolAyudante VARCHAR(15) PRIMARY KEY,
	nombre VARCHAR(45),
	apellido VARCHAR(45),
	cantidadSemestres INT
);

CREATE TABLE Mision(
	idMision SERIAL PRIMARY KEY,
	idProfesor INT REFERENCES Profesor(idProfesor),
	idAlumno VARCHAR(15) REFERENCES Alumno(rolAlumno),
	fechaIngreso TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	descripcion VARCHAR(90),
	recompensa VARCHAR(60),
	estado SMALLINT DEFAULT 0
);

CREATE TABLE Asignacion(
	idMision INT REFERENCES Mision(idMision),
	rolAyudante VARCHAR(15) REFERENCES Ayudante(rolAyudante),
	PRIMARY KEY (rolAyudante)
);