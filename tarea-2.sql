PGDMP                         x            tutdma     12.3 (Ubuntu 12.3-1.pgdg18.04+1)     12.3 (Ubuntu 12.3-1.pgdg18.04+1)     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16427    tutdma    DATABASE     x   CREATE DATABASE tutdma WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE tutdma;
                postgres    false            �           0    0    DATABASE tutdma    COMMENT     2   COMMENT ON DATABASE tutdma IS 'Tarea 2 Database';
                   postgres    false    2973            �            1259    16428    alumno    TABLE     �   CREATE TABLE public.alumno (
    rolalumno character varying(15) NOT NULL,
    nombre character varying(45),
    apellido character varying(45),
    anoingreso integer
);
    DROP TABLE public.alumno;
       public         heap    postgres    false            �            1259    16499 
   asignacion    TABLE     r   CREATE TABLE public.asignacion (
    idmision integer NOT NULL,
    rolayudante character varying(15) NOT NULL
);
    DROP TABLE public.asignacion;
       public         heap    postgres    false            �            1259    16475    ayudante    TABLE     �   CREATE TABLE public.ayudante (
    rolayudante character varying(15) NOT NULL,
    nombre character varying(45),
    apellido character varying(45),
    cantidadsemestres integer
);
    DROP TABLE public.ayudante;
       public         heap    postgres    false            �            1259    16482    mision    TABLE     .  CREATE TABLE public.mision (
    idmision integer NOT NULL,
    idprofesor integer,
    idalumno character varying(15),
    fechaingreso timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    descripcion character varying(90),
    recompensa character varying(60),
    estado smallint DEFAULT 0
);
    DROP TABLE public.mision;
       public         heap    postgres    false            �            1259    16480    mision_idmision_seq    SEQUENCE     �   CREATE SEQUENCE public.mision_idmision_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.mision_idmision_seq;
       public          postgres    false    206            �           0    0    mision_idmision_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.mision_idmision_seq OWNED BY public.mision.idmision;
          public          postgres    false    205            �            1259    16433    profesor    TABLE     �   CREATE TABLE public.profesor (
    idprofesor integer NOT NULL,
    nombre character varying(45),
    apellido character varying(45),
    especialidad character varying(45)
);
    DROP TABLE public.profesor;
       public         heap    postgres    false                       2604    16485    mision idmision    DEFAULT     r   ALTER TABLE ONLY public.mision ALTER COLUMN idmision SET DEFAULT nextval('public.mision_idmision_seq'::regclass);
 >   ALTER TABLE public.mision ALTER COLUMN idmision DROP DEFAULT;
       public          postgres    false    206    205    206            �          0    16428    alumno 
   TABLE DATA           I   COPY public.alumno (rolalumno, nombre, apellido, anoingreso) FROM stdin;
    public          postgres    false    202           �          0    16499 
   asignacion 
   TABLE DATA           ;   COPY public.asignacion (idmision, rolayudante) FROM stdin;
    public          postgres    false    207   *        �          0    16475    ayudante 
   TABLE DATA           T   COPY public.ayudante (rolayudante, nombre, apellido, cantidadsemestres) FROM stdin;
    public          postgres    false    204   G        �          0    16482    mision 
   TABLE DATA           o   COPY public.mision (idmision, idprofesor, idalumno, fechaingreso, descripcion, recompensa, estado) FROM stdin;
    public          postgres    false    206   d        �          0    16433    profesor 
   TABLE DATA           N   COPY public.profesor (idprofesor, nombre, apellido, especialidad) FROM stdin;
    public          postgres    false    203   �        �           0    0    mision_idmision_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.mision_idmision_seq', 21, true);
          public          postgres    false    205                       2606    16432    alumno alumno_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.alumno
    ADD CONSTRAINT alumno_pkey PRIMARY KEY (rolalumno);
 <   ALTER TABLE ONLY public.alumno DROP CONSTRAINT alumno_pkey;
       public            postgres    false    202                       2606    16503    asignacion asignacion_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY public.asignacion
    ADD CONSTRAINT asignacion_pkey PRIMARY KEY (rolayudante, idmision);
 D   ALTER TABLE ONLY public.asignacion DROP CONSTRAINT asignacion_pkey;
       public            postgres    false    207    207                       2606    16479    ayudante ayudante_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.ayudante
    ADD CONSTRAINT ayudante_pkey PRIMARY KEY (rolayudante);
 @   ALTER TABLE ONLY public.ayudante DROP CONSTRAINT ayudante_pkey;
       public            postgres    false    204                       2606    16488    mision mision_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.mision
    ADD CONSTRAINT mision_pkey PRIMARY KEY (idmision);
 <   ALTER TABLE ONLY public.mision DROP CONSTRAINT mision_pkey;
       public            postgres    false    206            	           2606    16437    profesor profesor_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.profesor
    ADD CONSTRAINT profesor_pkey PRIMARY KEY (idprofesor);
 @   ALTER TABLE ONLY public.profesor DROP CONSTRAINT profesor_pkey;
       public            postgres    false    203                       2606    16504 #   asignacion asignacion_idmision_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.asignacion
    ADD CONSTRAINT asignacion_idmision_fkey FOREIGN KEY (idmision) REFERENCES public.mision(idmision);
 M   ALTER TABLE ONLY public.asignacion DROP CONSTRAINT asignacion_idmision_fkey;
       public          postgres    false    207    2829    206                       2606    16509 &   asignacion asignacion_rolayudante_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.asignacion
    ADD CONSTRAINT asignacion_rolayudante_fkey FOREIGN KEY (rolayudante) REFERENCES public.ayudante(rolayudante);
 P   ALTER TABLE ONLY public.asignacion DROP CONSTRAINT asignacion_rolayudante_fkey;
       public          postgres    false    207    2827    204                       2606    16494    mision mision_idalumno_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mision
    ADD CONSTRAINT mision_idalumno_fkey FOREIGN KEY (idalumno) REFERENCES public.alumno(rolalumno);
 E   ALTER TABLE ONLY public.mision DROP CONSTRAINT mision_idalumno_fkey;
       public          postgres    false    206    202    2823                       2606    16489    mision mision_idprofesor_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.mision
    ADD CONSTRAINT mision_idprofesor_fkey FOREIGN KEY (idprofesor) REFERENCES public.profesor(idprofesor);
 G   ALTER TABLE ONLY public.mision DROP CONSTRAINT mision_idprofesor_fkey;
       public          postgres    false    2825    203    206            �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �     